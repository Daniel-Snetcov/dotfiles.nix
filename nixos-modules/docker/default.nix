{
  lib,
  config,
  user,
  pkgs,
  ...
}: {
  options = {
    dotfiles.docker.enable = lib.mkEnableOption "Enable Docker";
  };

  config = lib.mkIf config.dotfiles.docker.enable {
    virtualisation.docker.enable = true;
    users.users."${user}" = {
      extraGroups = ["docker"];
      packages = with pkgs; [
        docker-client
      ];
    };
  };
}
