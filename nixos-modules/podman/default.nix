{
  lib,
  config,
  user,
  pkgs,
  ...
}: {
  options = {
    dotfiles.podman.enable = lib.mkEnableOption "Enable Podman";
  };

  config = lib.mkIf config.dotfiles.podman.enable {
    virtualisation.podman = {
      enable = true;
      dockerCompat = true;
    };
    users.users."${user}" = {
      packages = with pkgs; [
        podman-desktop
      ];
    };
  };
}
