{
  lib,
  config,
  user,
  pkgs,
  inputs,
  ...
}: {
  options = {
    dotfiles.hyprland = {
      enable = lib.mkEnableOption "";
    };
  };
  config = lib.mkIf config.dotfiles.hyprland.enable {
    home-manager.users."${user}".dotfiles.hyprland.enable = true;
    services.xserver.enable = true;
    programs.hyprland = {
      enable = true;
      xwayland.enable = true;
    };
    users.users."${user}" = {
      extraGroups = ["wheel"];
      packages = [
        pkgs.hyprshot
        pkgs.clipse
        inputs.swww.packages."${pkgs.system}".swww
        inputs.neovim.packages.${pkgs.system}.default
        pkgs.xdg-desktop-portal-gtk
        pkgs.xdg-desktop-portal-hyprland
        pkgs.xwayland
      ];
    };
    environment.sessionVariables = {
      # Hint electron apps to use wayland
      NIXOS_OZONE_WL = "1";
      # XDG_CURRENT_DESKTOP = "Hyprland";
      # XDG_SESSION_TYPE = "wayland";
      # XDG_SESSION_DESKTOP = "Hyprland";
      MOZ_ENABLE_WAYLAND = "1";
      # T_QPA_PLATFORM = "wayland";
      # GDK_BACKEND = "wayland";
      # WLR_NO_HARDWARE_CURSORS = "1";

      # If your cursor becomes invisible
      # WLR_RENDERER_ALLOW_SOFTWARE = "1";
    };
    services.dbus.enable = true;
    xdg = {
      portal = {
        wlr.enable = true;
        enable = true;
        xdgOpenUsePortal = true;
        extraPortals = [
          pkgs.xdg-desktop-portal-gtk
          pkgs.xdg-desktop-portal-wlr
          pkgs.xdg-desktop-portal-hyprland
        ];
      };
    };

    security.rtkit.enable = true;
    security.pam.services.hyprlock = {};
  };
}
