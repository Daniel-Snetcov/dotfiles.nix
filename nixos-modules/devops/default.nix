{
  lib,
  config,
  user,
  pkgs,
  ...
}: {
  options = {
    dotfiles.bundles.devops.enable = lib.mkEnableOption "DevOps bundle";
  };

  config = lib.mkIf config.dotfiles.bundles.devops.enable {
    dotfiles.docker.enable = true;
    users.users."${user}" = {
      packages = with pkgs; [
        dig
        whois
        gnumake

        # Kubernetes
        kubectl
        kubeseal
        kubecolor
        (wrapHelm kubernetes-helm { plugins = [ kubernetes-helmPlugins.helm-diff ]; })
        helmfile
        minikube

        # grafana
        k6

        # Terraform
        terraform

        # Cloud Providers
        (google-cloud-sdk.withExtraComponents [google-cloud-sdk.components.gke-gcloud-auth-plugin])
        google-cloud-sql-proxy

        awscli2

        azure-cli
      ];
    };
    home-manager.users."${user}".programs.k9s.enable = true;
  };
}
