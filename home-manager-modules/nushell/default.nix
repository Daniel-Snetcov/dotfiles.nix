{
  lib,
  config,
  pkgs,
  ...
}: let
  inherit (pkgs) nu_scripts;
in {
  options = {
    dotfiles.nushell = {
      enable = lib.mkEnableOption "https://github.com/nushell/nushell";
    };
  };

  config = lib.mkIf config.dotfiles.nushell.enable {
    programs.nushell = {
      enable = true;
      shellAliases = {
        k = "${pkgs.kubecolor + "/bin/kubecolor"}";
        d = "${pkgs.doppler+ "/bin/doppler"} run -- ";
      };
      extraConfig = ''
        # completion
        use ${nu_scripts}/share/nu_scripts/custom-completions/git/git-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/glow/glow-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/just/just-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/make/make-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/man/man-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/nix/nix-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/cargo/cargo-completions.nu *
        use ${nu_scripts}/share/nu_scripts/custom-completions/zellij/zellij-completions.nu *
        # alias
        # use ${nu_scripts}/share/nu_scripts/aliases/git/git-aliases.nu *
        use ${nu_scripts}/share/nu_scripts/aliases/eza/eza-aliases.nu *
        use ${nu_scripts}/share/nu_scripts/aliases/bat/bat-aliases.nu *
        def ktx [ctx] {
          ${pkgs.kubectl + "/bin/kubectl"} config use-context $ctx
        }
      '';
    };
  };
}
