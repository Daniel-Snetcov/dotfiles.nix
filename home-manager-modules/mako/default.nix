{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.mako = {
      enable = lib.mkEnableOption "https://github.com/emersion/mako";
    };
  };

  config = lib.mkIf config.dotfiles.mako.enable {
    services.mako = {
      enable = true;
      defaultTimeout = 2000;
      borderRadius = 10;
      borderSize = 5;
    };
  };
}
