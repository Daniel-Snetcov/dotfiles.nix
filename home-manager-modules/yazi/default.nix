{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.yazi = {
      enable = lib.mkEnableOption "https://github.com/sxyazi/yazi";
    };
  };

  config = lib.mkIf config.dotfiles.yazi.enable {
    programs.yazi = {
      enable = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
      enableNushellIntegration = config.dotfiles.nushell.enable;
    };
  };
}
