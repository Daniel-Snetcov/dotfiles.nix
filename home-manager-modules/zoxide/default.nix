{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.zoxide = {
      enable = lib.mkEnableOption "https://github.com/ajeetdsouza/zoxide";
    };
  };

  config = lib.mkIf config.dotfiles.zoxide.enable {
    programs.zoxide = {
      enable = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
      enableNushellIntegration = config.dotfiles.nushell.enable;
    };
  };
}
