{
  lib,
  config,
  pkgs,
  ...
}: {
  options = {
    dotfiles.hypridle = {
      enable = lib.mkEnableOption "https://github.com/hyprwm/hypridle";
    };
  };

  config = lib.mkIf config.dotfiles.hypridle.enable {
    services.hypridle = {
      enable = true;
      settings = {
        general = {
          lock_cmd = "pidof hyprlock || hyprlock"; # avoid starting multiple hyprlock instances.
          before_sleep_cmd = "loginctl lock-session"; # lock before suspend.
          after_sleep_cmd = "hyprctl dispatch dpms on"; # to avoid having to press a key twice to turn on the display.
        };
        listener = [
          {
            timeout = 10;
            on-timeout = "pidof hyprlock || hyprlock";
          }
        ];
      };
    };
  };
}
