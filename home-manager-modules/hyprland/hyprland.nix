{
  lib,
  config,
  pkgs,
  ...
}: {
  options = {
    dotfiles.hyprland = {
      enable = lib.mkEnableOption "https://github.com/ajeetdsouza/hyprland";
    };
  };

  config = lib.mkIf config.dotfiles.hyprland.enable {
    wayland.windowManager.hyprland.enable = true;
    home.packages = with pkgs; [
      wl-clipboard
    ];
    dotfiles = {
      waybar.enable = true;
      mako.enable = true;
      hyprlock.enable = true;
      hypridle.enable = true;
    };
    xdg.portal = {
      enable = true;
      extraPortals = [pkgs.xdg-desktop-portal-gtk];
    };
  };
}
