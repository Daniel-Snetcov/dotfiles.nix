{
  lib,
  config,
  pkgs,
  ...
}: {
  options = {
    dotfiles.hyprlock = {
      enable = lib.mkEnableOption "https://github.com/hyprwm/hyprlock/";
    };
  };

  config = lib.mkIf config.dotfiles.hyprlock.enable {
    programs.hyprlock = {
      enable = true;
      settings = {
        general = {
          grace = 0;
        };

        background = [
          {
            path = "${config.dotfiles.wallpaper}";
            blur_passes = 1;
            blur_size = 3;
            brightness = 0.5;
            vibrancy = 0.2;
            vibrancy_darkness = 0.2;
          }
        ];

        input-field = [
          {
            size = "200, 50";
            position = "0, -80";
            dots_center = true;
            fade_on_empty = false;
            font_color = "rgb(202, 211, 245)";
            inner_color = "rgb(91, 96, 120)";
            outer_color = "rgb(24, 25, 38)";
            outline_thickness = 5;
            placeholder_text = "a$$word";
            shadow_passes = 2;
          }
        ];
      };
    };
  };
}
