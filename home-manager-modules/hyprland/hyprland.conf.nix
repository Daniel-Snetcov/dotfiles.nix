{
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  config = lib.mkIf config.dotfiles.hyprland.enable {
    wayland.windowManager.hyprland = let
      bindWorkspaces = [
        "$mod, a, workspace, 1"
        "$mod, s, workspace, 2"
        "$mod, d, workspace, 3"
        "$mod, f, workspace, 4"

        "$mod, j, workspace, 5"
        "$mod, k, workspace, 6"
        "$mod, l, workspace, 7"
        "$mod, semicolon, workspace, 8"

        "$mod SHIFT, a, movetoworkspace, 1"
        "$mod SHIFT, s, movetoworkspace, 2"
        "$mod SHIFT, d, movetoworkspace, 3"
        "$mod SHIFT, f, movetoworkspace, 4"

        "$mod SHIFT, j, movetoworkspace, 5"
        "$mod SHIFT, k, movetoworkspace, 6"
        "$mod SHIFT, l, movetoworkspace, 7"
        "$mod SHIFT, semicolon, movetoworkspace, 8"

        "$mod, u, workspace, 9"
        "$mod, i, workspace, 10"
        "$mod, o, workspace, 11"
        "$mod, p, workspace, 12"

        "$mod, m, workspace, 13"
        "$mod, comma, workspace, 14"
        "$mod, period, workspace, 15"
        "$mod, slash, workspace, 16"

        "$mod, q, workspace, 17"
        "$mod, w, workspace, 18"
        "$mod, e, workspace, 19"
        "$mod, r, workspace, 20"

        "$mod, left, movefocus, l"
        "$mod, right, movefocus, r"
        "$mod, up, movefocus, u"
        "$mod, down, movefocus, d"
      ];
    in {
      settings = {
        "$mod" = "SUPER";
        "$term" = "kitty";
        "$menu" = "anyrun";
        "$screenshot" = "hyprshot -m region --clipboard-only";

        bind =
          [
            "$mod, C, killactive"
            "$mod, Return, exec, $term"
            "$mod, SPACE, exec, $menu"
            ",Print, exec, $screenshot"
            "ALT, F, fullscreen"
            "SUPER, V, exec, $term --class clipse -e clipse"
          ]
          ++ bindWorkspaces;

        bindm = [
          "$mod, mouse:272, movewindow"
          "$mod, mouse:273, resizewindow"
        ];

        general = {
          gaps_in = 5;
          gaps_out = 20;
          border_size = 5;
          resize_on_border = false;
          allow_tearing = false;
          layout = "dwindle";
        };

        animations = {
          enabled = true;

          bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";

          animation = [
            "windows, 1, 7, myBezier"
            "windowsOut, 1, 7, default, popin 80%"
            "border, 1, 10, default"
            "borderangle, 1, 8, default"
            "fade, 1, 7, default"
            "workspaces, 1, 6, default"
          ];
        };
        decoration = {
          rounding = 10;
          active_opacity = 1.0;
          inactive_opacity = 0.75;
          drop_shadow = true;
          shadow_render_power = 3;
          blur = {
            enabled = true;
            size = 3;
            passes = 1;
            vibrancy = 0.1696;
          };
        };

        windowrulev2 = [
          "float, class:(clipse)"
          "size 622 652, class:(clipse)"

          "workspace 3, class:^(firefox)$"

          "workspace 17, class:^(vesktop)$"
          "workspace 19, initialTitle:^(Spotify)"
          "workspace 20, class:^(org.telegram.desktop)$"
          "float,class:(org.telegram.desktop),title:(Media viewer)"
        ];

        exec = [
          "swww img ${config.dotfiles.wallpaper}"
        ];
        exec-once = [
          "swww-daemon & sleep 0.1"
          "waybar"
          "clipse -clear"
          "clipse -listen"

          "[workspace 3 silent] firefox"
          "[workspace 4 silent] $term"

          "[workspace 5 silent] postman"
          # "[workspace 6 silent] datagrip"
          "[workspace 7 silent] mongodb-compass"

          "[workspace 17 silent] vesktop"
          "[workspace 19 silent] spotify"
          "[workspace 20 silent] telegram-desktop"

          "[workspace 9 silent] google-chrome-stable"
          "[workspace 10 silent] slack"
          "[workspace 11 silent] google-chrome-stable"
        ];

        input = {
          kb_layout = "us,ru";
          kb_options = "grp:alt_shift_toggle";
        };

        monitor =
          map
          (
            m: let
              resolution = "${toString m.width}x${toString m.height}@${toString m.refreshRate}";
              position = "${toString m.x}x${toString m.y}";
            in "${m.name},${
              if m.enabled
              then "${resolution},${position},1"
              else "disable"
            }"
          )
          config.dotfiles.monitors;
      };
    };
  };
}
