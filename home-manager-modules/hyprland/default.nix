{
  imports = [
    ./hyprlock.nix
    ./hypridle.nix
    ./hyprland.conf.nix
    ./hyprland.nix
  ];
}
