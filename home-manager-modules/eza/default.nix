{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.eza = {
      enable = lib.mkEnableOption "https://github.com/eza-community/eza";
    };
  };

  config = lib.mkIf config.dotfiles.eza.enable {
    programs.eza = {
      enable = true;
      icons = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
      enableNushellIntegration = config.dotfiles.nushell.enable;
    };
  };
}
