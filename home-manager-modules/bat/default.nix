{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.bat = {
      enable = lib.mkEnableOption "https://github.com/sharkdp/bat";
    };
  };

  config = lib.mkIf config.dotfiles.bat.enable {
    programs.bat = {
      enable = true;
    };
  };
}
