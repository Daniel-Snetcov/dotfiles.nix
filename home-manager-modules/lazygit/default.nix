{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.lazygit = {
      enable = lib.mkEnableOption "https://github.com/jesseduffield/lazygit";
    };
  };

  config = lib.mkIf config.dotfiles.lazygit.enable {
    programs.lazygit = {
      enable = true;
      settings = {
        git = {
          paging = {
            colorArg = "Always";
            pager = "delta --dark --paging=never";
          };
        };
      };
    };
  };
}
