{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.git = {
      enable = lib.mkEnableOption "Git";
      userName = lib.mkOption {
        type = lib.types.str;
        default = "";
      };
      userEmail = lib.mkOption {
        type = lib.types.str;
        default = "";
      };
    };
  };

  config = lib.mkIf config.dotfiles.git.enable {
    programs.git = {
      enable = true;
      inherit (config.dotfiles.git) userName userEmail;
      delta.enable = true;
      extraConfig = {
        pull.rebase = true;
        core.askpass = "";
        delta.navigate = true;
        merge.conflictstyle = "diff3";
        diff.conflictStyle = "default";
      };
    };
  };
}
