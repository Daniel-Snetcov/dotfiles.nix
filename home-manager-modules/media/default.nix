{
  lib,
  config,
  pkgs,
  ...
}: {
  options = {
    dotfiles.bundles.media = {
      enable = lib.mkEnableOption "Enable media applications bundle";
    };
  };

  config = lib.mkIf config.dotfiles.bundles.media.enable {
    home.packages = with pkgs; [
      firefox
      vesktop
      telegram-desktop
      spotify
      onlyoffice-bin

      slack
      google-chrome
    ];
  };
}
