{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.kitty = {
      enable = lib.mkEnableOption "https://github.com/kovidgoyal/kitty";
    };
  };

  config = lib.mkIf config.dotfiles.kitty.enable {
    programs.kitty = {
      enable = true;
    };
  };
}
