{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.starship = {
      enable = lib.mkEnableOption "https://github.com/starship/starship.git";
    };
  };

  config = lib.mkIf config.dotfiles.starship.enable {
    programs.starship = {
      enable = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
      enableNushellIntegration = config.dotfiles.nushell.enable;
      settings = {
        battery = {
          full_symbol = "🔋";
          charging_symbol = "🔌";
          discharging_symbol = "⚡";
        };
        character = {
          error_symbol = "[✖](bold red) ";
        };
        git_branch = {
          format = " [$symbol$branch]($style) ";
          symbol = "🍣 ";
          style = "bold yellow";
        };
        git_commit = {
          commit_hash_length = 8;
          style = "bold white";
        };
        gcloud = {
          disabled = true;
        };
      };
    };
  };
}
