{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.direnv = {
      enable = lib.mkEnableOption "https://github.com/direnv/direnv";
    };
  };

  config = lib.mkIf config.dotfiles.direnv.enable {
    programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
      enableNushellIntegration = config.dotfiles.nushell.enable;
    };
  };
}
