let
  # ["file1" "file2", "default.nix"]
  dirContent = builtins.attrNames (builtins.readDir ./.);
  notDefaultFile = path: path != "default.nix";

  # ["file1" "file2"]
  modules = builtins.filter notDefaultFile dirContent;

  # "file1" => { file1 = /path/to/file1 }
  collectModule = acc: curr: acc // { "${curr}" = ./. + "/${curr}"; };
in
  # {file1 = /path/to/file1; file2 = /path/to/file2 }
  builtins.foldl' collectModule {} modules
