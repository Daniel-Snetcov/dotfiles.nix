{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.btop = {
      enable = lib.mkEnableOption "https://github.com/aristocratos/btop";
    };
  };

  config = lib.mkIf config.dotfiles.btop.enable {
    programs.btop = {
      enable = true;
    };
  };
}
