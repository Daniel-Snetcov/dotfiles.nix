{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.waybar = {
      enable = lib.mkEnableOption "https://github.com/alexays/waybar";
    };
  };

  config = lib.mkIf config.dotfiles.waybar.enable {
    programs.waybar = {
      enable = true;
    };
  };
}
