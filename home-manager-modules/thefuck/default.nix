{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.thefuck = {
      enable = lib.mkEnableOption "https://github.com/nvbn/thefuck";
    };
  };

  config = lib.mkIf config.dotfiles.thefuck.enable {
    programs.thefuck = {
      enable = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
      enableNushellIntegration = config.dotfiles.nushell.enable;
    };
  };
}
