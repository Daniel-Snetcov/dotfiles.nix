{
  lib,
  ...
}: {
  options = {
    dotfiles.wallpaper = lib.mkOption {
      type = lib.types.path;
      default = ./wallhaven-6drjmq.png;
      description = "path to the wallpaper";
    };
  };
}
