{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.fzf = {
      enable = lib.mkEnableOption "https://github.com/junegunn/fzf";
    };
  };

  config = lib.mkIf config.dotfiles.fzf.enable {
    programs.fzf = {
      enable = true;
      # Not yet implemented
      # enableBashIntegration = config.dotfiles.bash.enable;
      # enableFishIntegration = config.dotfiles.fish.enable;
      # enableZshIntegration = config.dotfiles.zsh.enable;
    };
  };
}
