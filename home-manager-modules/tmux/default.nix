{
  lib,
  config,
  pkgs,
  ...
}: let
  tmuxBin = pkgs.tmux + "/bin/tmux";
  sessionizerScript = pkgs.pkgs.writeShellScriptBin "tzt" ''
    if [[ $# -eq 1 ]]; then
      selected=$(${pkgs.zoxide + "/bin/zoxide"} query -l $1)
    else
      selected=$(${pkgs.zoxide + "/bin/zoxide"} query -l | ${pkgs.fzf + "/bin/fzf"})
    fi

    if [[ -z $selected ]]; then
      exit 0
    fi

    selected_name=$(basename "$selected" | tr -c '[:alnum:]_' '_')

    if [[ -z $TMUX ]]; then
      if [[ -z $(pgrep tmux) ]]; then
        ${tmuxBin} new-session -s "$selected_name" -c "$selected" -d

        for i in {1..3}; do
          ${tmuxBin} new-window -t "$selected_name" -c "$selected"
        done

        ${tmuxBin} attach-session -t "$selected_name"
        exit 0
      else
        ${tmuxBin} attach
      fi
    else
      if ! ${tmuxBin} has-session -t="$selected_name" 2> /dev/null; then
        ${tmuxBin} new-session -ds "$selected_name" -c "$selected"

        for i in {1..3}; do
          ${tmuxBin} new-window -t "$selected_name" -c "$selected"
        done
      fi

      ${tmuxBin} switch-client -t "$selected_name"
    fi
  '';
in {
  options = {
    dotfiles.tmux = {
      enable = lib.mkEnableOption "https://github.com/tmux/tmux.git";
    };
  };

  config = lib.mkIf config.dotfiles.tmux.enable {
    programs.tmux = {
      enable = true;
      baseIndex = 1;
      prefix = "C-s";
      extraConfig = ''
        set -g status-position top
        set-window-option -g mode-keys vi

        # Switch panes like in Vim
        bind-key h select-pane -L
        bind-key j select-pane -D
        bind-key k select-pane -U
        bind-key l select-pane -R
      '';
    };
    home.packages = [
      sessionizerScript
    ];
  };
}
