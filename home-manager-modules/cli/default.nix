{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.bundles.cli = {
      enable = lib.mkEnableOption "CLI bundle";
    };
  };

  config = lib.mkIf config.dotfiles.bundles.cli.enable {
    dotfiles = {
      bat.enable = true;
      btop.enable = true;
      direnv.enable = true;
      fzf.enable = true;
      yazi.enable = true;
      eza.enable = true;
      thefuck.enable = true;
      git.enable = true;
      tmux.enable = true;
      starship.enable = true;
      lazygit.enable = true;
      zoxide.enable = true;
    };
  };
}
