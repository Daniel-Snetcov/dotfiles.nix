{
  lib,
  config,
  ...
}: {
  options = {
    dotfiles.firefox = {
      enable = lib.mkEnableOption "Firefox";
    };
  };

  config = lib.mkIf config.dotfiles.firefox.enable {
    programs.firefox = {
      enable = true;
    };
  };
}
