{
  config,
  lib,
  pkgs,
  inputs,
  user,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    ./stylix.nix
  ];

  dotfiles = {
    hyprland.enable = true;
    bundles.devops.enable = true;
  };
  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    crashDump.enable = true;
  };

  hardware.bluetooth.enable = true;
  hardware.keyboard.zsa.enable = true;

  networking = {
    hostName = "expertbook";
    networkmanager.enable = true;
    nameservers = ["1.1.1.1" "8.8.8.8" "8.8.4.4"];
  };

  time.timeZone = "Europe/Chisinau";

  users.users."${user}" = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "audio"
      "sound"
      "video"
      "networkmanager"
      "input"
      "tty"
    ];
    shell = pkgs.nushell;
    packages = with pkgs; [
      vim
      wget
      nh
      tldr
      bluetuith
      jetbrains.datagrip
      mongodb-compass
      postman
      doppler
      postgresql
      ffmpeg
      alsa-utils
      thunderbird
      easyeffects
      keymapp
    ];
  };

  sound.enable = true;
  services.pipewire = {
    enable = true;
    audio.enable = true;
    pulse.enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    wireplumber.enable = true;
  };
  systemd.user.services = {
    pipewire.wantedBy = ["default.target"];
    pipewire-pulse.wantedBy = ["default.target"];
  };
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  nixpkgs.config.allowUnfree = true;
  system.stateVersion = "24.05";
}
