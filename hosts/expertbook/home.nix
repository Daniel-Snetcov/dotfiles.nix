{
  config,
  pkgs,
  inputs,
  lib,
  user,
  ...
}: let
  homeManagerModules = builtins.attrValues inputs.self.homeManagerModules;
  builtinMonitor = {
    name = "eDP-1";
    width = 1920;
    height = 1080;
    refreshRate = 60;
    x = 0;
    y = 0;
    enabled = true;
  };
  dell240hz = {
    name = "HDMI-A-1";
    width = 1920;
    height = 1080;
    refreshRate = 120;
    x = 0;
    y = 0;
    enabled = true;
  };
  hp27q = {
    name = "HDMI-A-1";
    width = 2560;
    height = 1440;
    refreshRate = 60;
    x = 0;
    y = 0;
    enabled = true;
  };
in {
  imports =
    [
      inputs.anyrun.homeManagerModules.default
    ]
    ++ homeManagerModules;
  dotfiles = {
    bundles = {
      cli.enable = true;
      media.enable = true;
    };
    eza.enable = lib.mkForce false;
    kitty.enable = true;
    git = {
      userName = "Daniel Snetcov";
      userEmail = "daniel.snetcov@proton.me";
    };
    nushell.enable = true;
    anyrun.enable = true;

    monitors = [
      builtinMonitor
      hp27q
    ];
  };
  programs.home-manager.enable = true;
  home = {
    username = "${user}";
    homeDirectory = "/home/${user}";
    stateVersion = "24.05";
  };
}
