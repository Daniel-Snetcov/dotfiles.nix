inputs: let
  nixosModules = builtins.attrValues inputs.self.nixosModules;
  inherit (inputs.nixpkgs.lib) nixosSystem;
  user = "daniel";
in {
  expertbook = nixosSystem {
    system = "x86_64-linux";
    modules =
      [
        ./expertbook
        inputs.home-manager.nixosModules.home-manager
        inputs.stylix.nixosModules.stylix
        {
          home-manager = {
            extraSpecialArgs = {inherit inputs user;};
            useGlobalPkgs = true;
            useUserPackages = true;
            users."${user}" = ./expertbook/home.nix;
            backupFileExtension = "backup";
          };
        }
      ]
      ++ nixosModules;
    specialArgs = {inherit inputs user;};
  };
}
